# Makefile

## blends-images(7) - Pure Blends System Image Configurations
## Copyright (C) 2006-2014 Daniel Baumann <mail@daniel-baumann.ch>
## Copyright (C) 2014 Iain R. Learmonth <irl@fsfe.org>
##
## This program comes with ABSOLUTELY NO WARRANTY; for details on terms of
## use, modification and redistribution, see debian/copyright.

SHELL := sh -e

#SCRIPTS = images/*/auto/* images/*/config/hooks/*
SCRIPTS = images/*/auto/*

all: build

test:
	@echo -n "Checking for syntax errors"

	@for SCRIPT in $(SCRIPTS); \
	do \
		sh -n $${SCRIPT}; \
		echo -n "."; \
	done

	@echo " done."

	@if [ -x "$$(which checkbashisms 2>/dev/null)" ]; \
	then \
		echo -n "Checking for bashisms"; \
		for SCRIPT in $(SCRIPTS); \
		do \
			checkbashisms -f -x $${SCRIPT}; \
			echo -n "."; \
		done; \
		echo " done."; \
	else \
		echo "W: checkbashisms - command not found"; \
		echo "I: checkbashisms can be obtained from: "; \
		echo "I:   http://git.debian.org/?p=devscripts/devscripts.git"; \
		echo "I: On Debian based systems, checkbashisms can be installed with:"; \
		echo "I:   apt-get install devscripts"; \
	fi

build:
	@echo "Nothing to build."

install:
	# Installing image configurations
	mkdir -p $(DESTDIR)/usr/share/blends
	mkdir -p $(DESTDIR)/usr/share/doc
	mkdir -p $(DESTDIR)/usr/share/man
	cp -a images $(DESTDIR)/usr/share/blends

clean:
	@echo "Nothing to clean."

distclean: clean
	@echo "Nothing to distclean."

reinstall: uninstall install
